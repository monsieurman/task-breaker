export enum TaskStatus {
  Completed = 'Completed',
  Cancelled = 'Cancelled',
  Pending = 'Pending'
}

export interface Task {
  content: string;
  createdAt: Date;
  updatedAt: Date;
  status: TaskStatus;
  id: string;
  list: string;
}
