import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule, MatInputModule, MatCardModule, MatButtonModule } from '@angular/material';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { reducers } from './state/reducers';
import { TaskListComponent } from './containers/task-list/task-list.component';
import { TaskComponent } from './components/task/task.component';
import { TaskEffect } from './state/task.effect';
import { TimeAgoPipe } from './pipes/time-ago.pipe';

const matModules = [
  MatIconModule,
  MatInputModule,
  MatCardModule,
  MatButtonModule
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    StoreModule.forFeature('core', reducers),
    EffectsModule.forFeature([TaskEffect]),

    ...matModules
  ],
  declarations: [TaskListComponent, TaskComponent, TimeAgoPipe],
  exports: [TaskListComponent]
})
export class CoreModule { }
