import { Component, OnInit, Input, ChangeDetectorRef, OnDestroy, ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';
import { Task, TaskStatus } from '../../models/Task';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskComponent implements OnInit, OnDestroy {

  private intervalRef: any;

  @Input()
  task: Task;

  @Input()
  selected = false;

  @Output()
  toggle = new EventEmitter<void>();

  constructor(private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.intervalRef = setInterval(() => {
      this.changeDetectorRef.markForCheck();
    }, 5000);
  }

  ngOnDestroy() {
    clearInterval(this.intervalRef);
  }

  emitToggle() {
    this.toggle.emit();
  }

  getIcon(): string {
    switch (this.task.status) {
      case TaskStatus.Completed:
        return 'done';
      case TaskStatus.Cancelled:
        return 'close';
      case TaskStatus.Pending:
        return 'check_box_outline_blank';
    }
  }

  getStatus(): string[] {
    const classes = [this.getCompletionStatus()];
    if (this.selected) {
      classes.push('app-task-selected');
    }
    return classes;
  }

  private getCompletionStatus() {
    switch (this.task.status) {
      case TaskStatus.Completed:
        return 'app-task-completed';
      case TaskStatus.Cancelled:
        return 'app-task-cancelled';
      case TaskStatus.Pending:
        return 'app-task-pending';
    }
  }
}
