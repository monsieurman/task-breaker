import { Action } from '@ngrx/store';

import { Task } from '../models/Task';

export enum TaskActionTypes {
  Create = '[Task] Create',
  Complete = '[Task] Complete',
  Cancel = '[Task] Cancel',
  MarkAsPending = '[Task] Mark as pending',
  Delete = '[Task] Delete',
  Load = '[Task] Load',
  CreateList = '[List] Create',
  DeleteList = '[List] Delete',
  SelectList = '[List] Select'
}

export class CreateTask implements Action {
  readonly type = TaskActionTypes.Create;
  /** @param payload Task content */
  constructor(public payload: { parentListID: string, taskContent: string }) { }
}

export class CompleteTask implements Action {
  readonly type = TaskActionTypes.Complete;
  /** @param payload Task ID */
  constructor(public payload: string) { }
}

export class CancelTask implements Action {
  readonly type = TaskActionTypes.Cancel;
  /** @param payload Task ID */
  constructor(public payload: string) { }
}

export class MarkTaskAsPending implements Action {
  readonly type = TaskActionTypes.MarkAsPending;
  /** @param payload Task ID */
  constructor(public payload: string) { }
}

export class DeleteTask implements Action {
  readonly type = TaskActionTypes.Delete;
  /** @param payload Task ID */
  constructor(public payload: string) { }
}

export class LoadTasks implements Action {
  readonly type = TaskActionTypes.Load;
  /** @param payload new tasks */
  constructor(public payload: Task[]) { }
}

export class CreateList implements Action {
  readonly type = TaskActionTypes.CreateList;
  /** @param payload list name */
  constructor(public payload: string) { }
}

export class SelectList implements Action {
  readonly type = TaskActionTypes.SelectList;
  /** @param payload list id */
  constructor(public payload: string) { }
}

export class DeleteList implements Action {
  readonly type = TaskActionTypes.DeleteList;
  /** @param payload list id */
  constructor(public payload: string) { }
}

export type TaskActions =
  | CreateTask
  | CompleteTask
  | CancelTask
  | MarkTaskAsPending
  | DeleteTask
  | LoadTasks
  | CreateList
  | DeleteList
  | SelectList;
