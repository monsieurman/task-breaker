import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import { tasksReducer, TaskState } from './task.reducer';
import { Task, TaskStatus } from '../models/Task';

export interface CoreModuleState {
  task: TaskState;
}

export const reducers: ActionReducerMap<CoreModuleState> = {
  task: tasksReducer,
};

export const coreModuleSelector = createFeatureSelector<CoreModuleState>('core');

export const getAllTasks = createSelector(
  coreModuleSelector,
  (state: CoreModuleState) => state.task.tasks
);

export const getTaskInCurrentList = createSelector(
  coreModuleSelector,
  (state: CoreModuleState) => state.task.tasksInSelectedList
);

export const getListState = createSelector(
  coreModuleSelector,
  (state: CoreModuleState) => state.task
);

export const getLists = createSelector(
  coreModuleSelector,
  (state: CoreModuleState) => state.task.lists
);

export const getSelectedListID = createSelector(
  coreModuleSelector,
  (state: CoreModuleState) => state.task.selectedListID
);

export const getPendingTasks = createSelector(
  getAllTasks,
  (tasks: Task[]) => tasks.filter(task => task.status === TaskStatus.Pending)
);

export const getNonPendingTasks = createSelector(
  getAllTasks,
  (tasks: Task[]) => tasks.filter(task => task.status !== TaskStatus.Pending)
);
