import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';

import { CoreModuleState, getListState, getTaskInCurrentList } from '../../state/reducers';
import { Task, TaskStatus } from 'src/app/core/models/Task';
import { MarkTaskAsPending, CompleteTask, CancelTask, CreateTask, DeleteTask } from '../../state/task.action';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  tasks: Task[];
  pendingTasks: Task[];
  nonPendingTasks: Task[];
  selectedListID: string;
  selectedListName: string;
  selectedIndex = 0;
  taskInputFocused = false;

  @ViewChild('taskInput')
  input: ElementRef<HTMLInputElement>;

  constructor(
    private store: Store<CoreModuleState>
  ) {
    store.select(getListState).subscribe(listState => {
      this.selectedListID = listState.selectedListID;
      this.selectedListName = listState.lists.find(list => list.id === listState.selectedListID).name;
    });
    store.select(getTaskInCurrentList).subscribe(tasks => {
      this.tasks = tasks.filter(task => task.list === this.selectedListID);
      this.pendingTasks = this.tasks.filter(task => task.status === TaskStatus.Pending);
      this.nonPendingTasks = this.tasks.filter(task => task.status !== TaskStatus.Pending);

      if (this.selectedIndex > tasks.length - 1) {
        this.selectedIndex = tasks.length - 1;
      }
    });
  }

  ngOnInit() {
  }

  onFocus() {
    this.taskInputFocused = true;
  }

  onBlur() {
    this.taskInputFocused = false;
  }

  submit() {
    const inputValue = this.input.nativeElement.value;
    if (inputValue !== '') {
      this.store.dispatch(new CreateTask({ taskContent: inputValue, parentListID: this.selectedListID }));
      this.input.nativeElement.value = '';
    }
  }

  // TODO: refactor this ugly thing !
  @HostListener('document:keydown', ['$event'])
  onkeypress(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      this.blurInput();
      return;
    }
    if (event.key === '/') {
      this.toggleInputFocus(event);
    } else if (this.taskInputFocused || this.tasks.length === 0) {
      return;
    }
    switch (event.key) {
      case 'j': // j
        this.selectNextTask();
        break;
      case 'k': // h
        this.selectPreviousTask();
        break;
      case ' ':
        this.toggleSelectedTask();
        break;
      case 'c':
        this.cancelSelectedTask();
        break;
      case 'd':
        this.deleteSelectedTask();
        break;
    }
  }

  private selectNextTask() {
    if (this.tasks.length - 1 > this.selectedIndex) {
      this.selectedIndex++;
    }
  }

  private selectTask(index: number) {
    this.selectedIndex = index;
  }

  private selectPreviousTask() {
    if (this.selectedIndex > 0) {
      this.selectedIndex--;
    }
  }

  private toggleSelectedTask() {
    const task = this.tasks[this.selectedIndex];
    this.toggleTask(task.id);
  }

  toggleTask(id: string) {
    const task = this.tasks.find(t => t.id === id);
    const action = task.status === TaskStatus.Completed || task.status === TaskStatus.Cancelled
      ? new MarkTaskAsPending(task.id)
      : new CompleteTask(task.id);
    this.store.dispatch(action);
  }

  private deleteSelectedTask() {
    const task = this.tasks[this.selectedIndex];
    this.store.dispatch(new DeleteTask(task.id));
  }

  private cancelSelectedTask() {
    const task = this.tasks[this.selectedIndex];
    this.store.dispatch(new CancelTask(task.id));
  }

  private toggleInputFocus(event: KeyboardEvent) {
    if (this.taskInputFocused) {
      this.blurInput();
    } else {
      this.focusInput();
    }
    event.preventDefault();
  }

  private focusInput() {
    this.input.nativeElement.focus();
    this.taskInputFocused = true;
  }

  private blurInput() {
    this.taskInputFocused = false;
    this.input.nativeElement.blur();
    this.input.nativeElement.value = '';
  }
}
